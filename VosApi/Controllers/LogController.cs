﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VosApi.Model;

namespace VosApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly LogContext _context;

        public LogController(LogContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        // GET: api/log
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LogItem>>> GetLogItems()
        {
            return await _context.LogItems.ToListAsync();
        }

        // GET: api/log/id
        [HttpGet("{id}")]
        public async Task<ActionResult<LogItem>> GetLogItem(long id)
        {
            var logItem = await _context.LogItems.FindAsync(id);
            if(logItem == null)
            {
                return NotFound();
            }
            return logItem;
        }

        // POST: api/log
        [HttpPost]
        public async Task<ActionResult<LogItem>> CreateLogItem(LogItem logItem)
        {
            _context.LogItems.Add(logItem);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetLogItem", new { id = logItem.Id }, logItem);
        }
    }
}