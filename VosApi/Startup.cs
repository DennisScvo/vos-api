﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VosApi.Model;

namespace VosApi
{
    public class Startup
    {
        string ConnectionString = "";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<LogContext>(opt =>
                opt.UseSqlServer(Configuration.GetConnectionString(ConnectionString)));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsEnvironment("Local"))
            {
                ConnectionString = "Local";
            }
            else
            {
                ConnectionString = "Cloud";
            }
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
