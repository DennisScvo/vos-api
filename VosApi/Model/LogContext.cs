﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VosApi.Model
{
    public class LogContext : DbContext
    {
        public LogContext(DbContextOptions<LogContext> options) : base(options) { }

        public DbSet<LogItem> LogItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<LogItem>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.User);
                entity.Property(e => e.Email);
                entity.Property(e => e.Rol);
                entity.Property(e => e.ProcedureTitel).IsRequired();
                entity.Property(e => e.ProcedureCode).IsRequired();
                entity.Property(e => e.StapTitle).IsRequired();
                entity.Property(e => e.TargetTelefoonnummer).IsRequired();
                entity.Property(e => e.SourceTelefoonnummer);
                entity.Property(e => e.Timestamp).IsRequired();
            });
        }
    }
}
