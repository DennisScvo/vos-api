﻿namespace VosApi.Model
{
    public class LogItem
    {
        public long Id { get; set; } 
        public string User { get; set; }
        public string Email { get; set; }
        public string Rol { get; set; }
        public string ProcedureTitel { get; set; }
        public string ProcedureCode { get; set; }
        public string StapTitle { get; set; }
        public string TargetTelefoonnummer { get; set; }
        public string SourceTelefoonnummer { get; set; }
        public long Timestamp { get; set; }
    }
}
